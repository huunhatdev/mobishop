﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static MobiShop.pareProfile;
using static MobiShop.Package;
using System.IO;

namespace RegMobifone
{
    class exMobiShop
    {

        public static string urlMobishop = "https://api.mobishop.vn";
      

        public static void Login(string username, string password, out bool hasToken, out string token, string proxy = null)
        {
            hasToken = false;
            token = string.Empty;
            try
            {

                using (HttpClientHandler handler = new HttpClientHandler())
                {

                    handler.CookieContainer = new System.Net.CookieContainer();
                    handler.UseCookies = false;
                    handler.AutomaticDecompression = System.Net.DecompressionMethods.GZip | System.Net.DecompressionMethods.Deflate | System.Net.DecompressionMethods.None;

                    if (proxy != null)
                    {
                        var webProxy = new WebProxy(proxy);
                        //{
                        //    username và password proxy nếu có
                        //    Credentials = new NetworkCredential("username", "password"),
                        //};
                        handler.UseProxy = true;
                        handler.Proxy = webProxy;
                    }
                    using (HttpClient client = new HttpClient(handler))
                    {
                        client.DefaultRequestHeaders.Clear();
                        //client.DefaultRequestHeaders.TryAddWithoutValidation("type", "agent");
                        Dictionary<string, string> body = new Dictionary<string, string>();
                        body.Add("type", "agent");
                        body.Add("phone", username);
                        body.Add("password", password);
                        var request = client.PostAsync($"{urlMobishop}/api/auth/login", new FormUrlEncodedContent(body)).Result;
                        string response = request.Content.ReadAsStringAsync().Result;
                        try
                        {
                            token = JObject.Parse(response).SelectToken("token").Value<string>();
                            hasToken = true;
                            //get profile
                            client.DefaultRequestHeaders.TryAddWithoutValidation("authorization", token);
                            client.DefaultRequestHeaders.TryAddWithoutValidation("accept-encoding", "gzip, deflate, br");
                            response = client.GetStringAsync($"{urlMobishop}/api/agent/profile").Result;
                            Profile profile = JsonConvert.DeserializeObject<Profile>(response);
                            int id = profile.data.id;
                            string name = profile.data.name;
                            string hoahong = profile.data.commissionPrice;

                        }
                        catch
                        {
                            token = JObject.Parse(response).SelectToken("message").Value<string>();
                        }
                    }
                }
            }
            catch
            {
            }
        }

        public static string getProfile(string token, string proxy = null)
        {
            string data = string.Empty;
            try
            {

                using (HttpClientHandler handler = new HttpClientHandler())
                {

                    handler.CookieContainer = new System.Net.CookieContainer();
                    handler.UseCookies = false;
                    handler.AutomaticDecompression = System.Net.DecompressionMethods.GZip | System.Net.DecompressionMethods.Deflate | System.Net.DecompressionMethods.None;

                    if (proxy != null)
                    {
                        var webProxy = new WebProxy(proxy);
                        //{
                        //    username và password proxy nếu có
                        //    Credentials = new NetworkCredential("username", "password"),
                        //};
                        handler.UseProxy = true;
                        handler.Proxy = webProxy;
                    }
                    using (HttpClient client = new HttpClient(handler))
                    {
                        client.DefaultRequestHeaders.Clear();
                        client.DefaultRequestHeaders.TryAddWithoutValidation("authorization", token);
                        client.DefaultRequestHeaders.TryAddWithoutValidation("accept-encoding", "gzip, deflate, br");
                        data = client.GetStringAsync($"{urlMobishop}/api/agent/profile").Result;
                     

                    }
                }
                return data;
            }
            catch
            {
                return data;
            }
        }

        public static string sendPackage(string phoneNumber, string package,string agent, string token,out bool isSuccess, string proxy = null)
        {
            isSuccess = false;
            string  msg = string.Empty;
            try
            {

                using (HttpClientHandler handler = new HttpClientHandler())
                {

                    handler.CookieContainer = new System.Net.CookieContainer();
                    handler.UseCookies = false;
                    handler.AutomaticDecompression = System.Net.DecompressionMethods.GZip | System.Net.DecompressionMethods.Deflate | System.Net.DecompressionMethods.None;

                    if (proxy != null)
                    {
                        var webProxy = new WebProxy(proxy);
                        //{
                        //    username và password proxy nếu có
                        //    Credentials = new NetworkCredential("username", "password"),
                        //};
                        handler.UseProxy = true;
                        handler.Proxy = webProxy;
                    }
                    using (HttpClient client = new HttpClient(handler))
                    {
                        client.DefaultRequestHeaders.Clear();
                        client.DefaultRequestHeaders.TryAddWithoutValidation("authorization", token);
                        client.DefaultRequestHeaders.TryAddWithoutValidation("auth-token", token);
                        client.DefaultRequestHeaders.TryAddWithoutValidation("accept-encoding", "gzip, deflate, br");
                        Dictionary<string, string> body = new Dictionary<string, string>();
                        body.Add("type", "packagemobile");
                        body.Add("agent", agent);
                        body.Add("phone", phoneNumber);
                        body.Add("products[0][id]", Packages[package].ToString());
                        body.Add("products[0][count]", "1");
                        var request = client.PostAsync($"{urlMobishop}/api/commit-order", new FormUrlEncodedContent(body)).Result;
                        string response = request.Content.ReadAsStringAsync().Result.ToString() ;
                        try
                        {
                            msg = JObject.Parse(response).SelectToken("order").Value<string>();
                            isSuccess = true;

                        }
                        catch
                        {
                            msg =  DecodeUTF8(response);
                            msg = JObject.Parse(response).SelectToken("massage").Value<string>();
                        }

                    }
                }
                return msg;
            }
            catch
            {
                return msg;
            }


        }

        public static string DecodeUTF8(string text)
        {
            UnicodeEncoding Unicode = new UnicodeEncoding();
            byte[] FileTextBytes = Unicode.GetBytes(text);
            string result = Unicode.GetString(FileTextBytes);
            return result;
        }
      
    }
}
         
