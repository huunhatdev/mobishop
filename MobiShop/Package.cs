﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobiShop
{
    class Package
    {
        public static  Dictionary<string, int> Packages = new Dictionary<string, int>()
        {
            {"12FV99" , 120871},
            {"6FV99" , 120870},
            {"3FV99" , 120867},
            {"Y60" , 120862},
            {"6CV119" , 120861},
            {"MAX90" , 120860},
            {"S50" , 120855},
            {"3CV119" , 120620},
            {"12CV99" , 120609},
            {"6CV99" , 120600},
            {"THAGA100" , 120584},
            {"48THAGA7" , 120563},
            {"24THAGA7" , 120560},
            {"4THAGA7" , 120405},
            {"2THAGA7" , 120404},
            {"THAGA7" , 120403},
            {"CS" , 68702},
            {"6WIFI" , 52657},
            {"12WIFI" , 52654},
            {"FD50" , 81569},
            {"12CS" , 68721},
            {"6CS" , 68716},
            {"3CS" , 68709},
            {"FV99" , 67477},
            {"CV99" , 67476},
            {"CV119" , 67472},
            {"3CV99" , 67469},
            {"12CV119" , 67468},
            {"GV119" , 67465},
            {"12GV99" , 67462},
            {"6GV99" , 67461},
            {"3GV99" , 67458},
            {"6GV119" , 67453},
            {"9GV119" , 67427},
            {"12GV119" , 67424},
            {"9GV99" , 67419},
            {"GV99" , 67416},
            {"3GV119" , 67410},
            {"C120N" , 62641},
            {"12DATA5" , 62640},
            {"6DATA5" , 62639},
            {"DATA5" , 62637},
            {"C50N" , 59944},
            {"12Z70" , 59919},
            {"12THAGA5" , 59918},
            {"6Z70" , 59913},
            {"6CS50" , 59912},
            {"12ED100" , 59911},
            {"6ED100" , 59908},
            {"3ED100" , 59905},
            {"ED100" , 59902},
            {"24G" , 59901},
            {"12S50" , 59900},
            {"6S50" , 59898},
            {"3S50" , 59897},
            {"2S50" , 59896},
            {"12Y60" , 59895},
            {"6Y60" , 59894},
            {"3Y60" , 59891},
            {"12THAGA102" , 59888},
            {"Z70" , 59883},
            {"3MAX100" , 59860},
            {"3Z70" , 59851},
            {"MAX100" , 59844},
            {"21G" , 59823},
            {"3C90N" , 59822},
            {"C490" , 59819},
            {"C390" , 59816},
            {"C290" , 59805},
            {"C190" , 59802},
            {"6CL50" , 59801},
            {"3CL50" , 59798},
            {"CL50" , 59797},
            {"12THAGA7" , 59792},
            {"SD" , 58805},
            {"LD" , 58796},
            {"3MAX90" , 52772},
            {"3C50N" , 52765},
            {"3C120" , 52758},
            {"24GIP12" , 52756},
            {"24GIP6" , 52753},
            {"24GIP3" , 52750},
            {"24GIP" , 52746},
            {"24GDIP12" , 52743},
            {"24GDIP6" , 52742},
            {"24GDIP3" , 52739},
            {"24GDIP" , 52726},
            {"TS4G" , 52725},
            {"3TS4G" , 52724},
            {"6TS4G" , 52722},
            {"ED50" , 52720},
            {"3ED50" , 52716},
            {"6ED50" , 52713},
            {"12ED50" , 52709},
            {"12C90N" , 52702},
            {"6C90N" , 52698},
            {"18WF" , 52673},
            {"12WF" , 52672},
            {"6WF" , 52671},
            {"WF" , 52668},
            {"MBH6" , 52665},
            {"MBH12" , 52658},
            {"12MAX90" , 50166},
            {"6MAX90" , 50165},
            {"12C50N" , 50162},
            {"6C50N" , 50161},
            {"CS3N" , 23023},
            {"CS6N" , 23018},
            {"12HD300N" , 22019},
            {"12HD200N" , 22018},
            {"12HD120N" , 22017},
            {"12HD90N" , 22016},
            {"12HD70N" , 22013},
            {"12TS4G" , 21783},
            {"12C120" , 21781},
            {"6C120" , 21780},
            {"C120" , 21778},
            {"HD500" , 21656},
            {"HD400" , 21655},
            {"HD300" , 21654},
            {"HD200" , 21653},
            {"HD120" , 21652},
            {"HD90" , 21651},
            {"HD70" , 21650},
            {"21G12" , 21648},
            {"21G6" , 21647},
            {"21G3" , 21646},
            {"24G12" , 21645},
            {"24G6" , 21642},
            {"24G3" , 21641},
            {"12BL5G" , 13406},
            {"10BL5G" , 13403},
            {"6BL5G" , 13400},
            {"BL5G" , 13397},
            {"12GIAITRI5" , 13396},
            {"6GIAITRI5" , 13391},
            {"3GIAITRI5" , 13390},
            {"GIAITRI5" , 13389},
            {"12FD50" , 13384},
            {"6FD50" , 13381},
            {"3FD50" , 13345},
            {"THAGA5" , 13219},
            {"24THAGA15" , 13218},
            {"12THAGA15" , 13217},
            {"12THAGA100" , 13215},
            {"9THAGA100" , 13214},
            {"6THAGA100" , 13213},
            {"3THAGA100" , 13212},
            //{"THAGA100" , 13211},
            {"D15" , 7778},
            {"12DTHN" , 535},
            {"6DTHN" , 534},
            {"3DTHN" , 533},
            {"DTHN" , 532},
            {"12CS120" , 531},
            {"6CS120" , 530},
            {"3CS120" , 527},
            {"CS120" , 524},
            {"D30" , 140},
            {"C90N" , 22966},

        };

      

       

    }
}
