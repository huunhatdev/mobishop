﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobiShop
{
    class pareProfile
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
        public class Data
        {
            public int id { get; set; }
            public bool sell { get; set; }
            public object avatar { get; set; }
            public string name { get; set; }
            public string phone { get; set; }
            public string identity_card { get; set; }
            public string bank_id { get; set; }
            public string bank_number { get; set; }
            public int city { get; set; }
            public int district { get; set; }
            public object ward { get; set; }
            public List<string> images { get; set; }
            public int point { get; set; }
            public string pointString { get; set; }
            public string commissionPrice { get; set; }
            public object commissionNumber { get; set; }
            public string type { get; set; }
        }

        public class Profile
        {
            public Data data { get; set; }
        }


    }
}
