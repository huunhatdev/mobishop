﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MobiShop
{
    class VPS
    {
        public string Msg { get; set; }
        public string RequestID { get; set; }
        public string CustFullName { get; set; }
        //ho va ten
        public string CustMobilePhone { get; set; }
        //sdt
        public string CustEmail { get; set; }
        //email
        public string Consultant { get; set; }
        //ma gioi thieu
        public string RefName { get; set; }
        //ten nguoi gioi thieu
        public string CardFrontImage { get; set; }
        //cmnd mat truoc
        public string CardBackImage { get; set; }
        //cmnd mat sau
        public string ConsultantRefer { get; set; }
        // null
        public string Source { get; set; }
        //null
        public string CustBornDate { get; set; }
        //ngay sinh
        public string Gender { get; set; }
        //gioi tinh MALE FEMALE
        public string CardId { get; set; }
        //cmnd
        public string CardIssuedDate { get; set; }
        //ngay cap
        public string CardExpireDate { get; set; }
        //ngya het han
        public string CardIssuedPlace { get; set; }
        //noi cap
        public string CustAddress { get; set; }
        //dia chi
        public string IpAccepted
        {
            get
            {
                return "on";
            }
        } 
        public string URI
        {
            get
            {
                return "https://openaccount.vps.com.vn";
            }
        }
        public string CustAddrProvince { get; set; }
        //tinh thanh
          public string IDSuccess { get; set; }
        //id thanh cong

        public string Proxy { get; set; }
        //Proxy

        // default constructor


        //CREAT Request ID
        private static Random random = new Random();

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public string CreatRequestID()
        {
            string id = $"{RandomString(8)}-{RandomString(4)}-{RandomString(4)}-{RandomString(4)}-{RandomString(12)}";
            RequestID = id;
            return id;
        }

        // process info step 3
        public bool CardInfo(string text)
        {
            Msg = string.Empty;
            var o = JObject.Parse(text);
            bool isSuccess = o.SelectToken("success").Value<bool>();
            if (isSuccess)
            {
                string view = o.SelectToken("viewContent").ToString();
                string decode = System.Net.WebUtility.HtmlDecode(view);

                //regex
                string _CustBornDate = "(?:CustBornDate.+value=\")([\\d\\/]+).";
                string _CardId = "(?:CardId.+value=\")([\\d]+).";
                string _CardIssuedDate = "(?:CardIssuedDate.+value=\")([\\d\\/]+).";
                string _CardExpireDate = "(?:CardExpireDate.+value=\")([\\d\\/]+).";
                string _CardIssuedPlace = "(?:CardIssuedPlace.+=\")(.+)\"\\s\\splace";
                string _CustAddress = "(?:CustAddress.+=\")(.+)\"\\splace";
                string _CustAddrProvince = $"(?:value=\")([\\w\\_] +)\"\\s\\>(?:{_CustAddress})";
                CustBornDate = Regex.Match(decode, _CustBornDate).Groups[1].ToString();
                CardId = Regex.Match(decode, _CardId).Groups[1].ToString();
                CardIssuedDate = Regex.Match(decode, _CardIssuedDate).Groups[1].ToString();
                CardExpireDate = Regex.Match(decode, _CardExpireDate).Groups[1].ToString();
                CardIssuedPlace = Regex.Match(decode, _CardIssuedPlace).Groups[1].ToString();
                CustAddress = Regex.Match(decode, _CustAddress).Groups[1].ToString();
                CustAddrProvince = Regex.Match(decode, _CustAddrProvince).Groups[1].ToString();
                return true;
            }
            else
            {
                Msg = o.SelectToken("message").ToString();
            }
            return false;
        }

        // register success

        public bool isRegisterSuccess(string text)
        {
            Msg = string.Empty;
            var o = JObject.Parse(text);
            bool isSuccess = o.SelectToken("success").Value<bool>();
            if (isSuccess)
            {
                string view = o.SelectToken("viewContent").ToString();
                string decode = System.Net.WebUtility.HtmlDecode(view).Replace("\r\n", "");

                //regex
                string _IDSuccess = "(?:CustBornDate.+value=\")([\\d\\/]+).";
                IDSuccess = Regex.Match(decode, _IDSuccess).Groups[1].ToString();
                return true;
            }
            else
            {
                Msg = o.SelectToken("message").ToString();
            }
            return false;
        }

        //gender 0: nam 1:nu
        public bool Step1(string fullName, string phoneNumber, int gender, string email, string consultant, string refName)
        {
            Msg = string.Empty;
            RequestID = CreatRequestID();
            using (HttpClientHandler handler = new HttpClientHandler())
            {

                handler.CookieContainer = new System.Net.CookieContainer();
                handler.UseCookies = false;
                handler.AutomaticDecompression = System.Net.DecompressionMethods.GZip | System.Net.DecompressionMethods.Deflate | System.Net.DecompressionMethods.None;

                if (string.IsNullOrEmpty(Proxy))
                {
                    var webProxy = new WebProxy(Proxy);
                    //{
                    //    username và password proxy nếu có
                    //    Credentials = new NetworkCredential("username", "password"),
                    //};
                    handler.UseProxy = true;
                    handler.Proxy = webProxy;
                }
                using (HttpClient client = new HttpClient(handler))
                {
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.TryAddWithoutValidation("accept-encoding", "gzip, deflate, br");
                    Dictionary<string, string> body = new Dictionary<string, string>();
                    body.Add("StepCount", "2");
                    body.Add("isNext", "true");
                    body.Add("RequestID", RequestID);
                    body.Add("IsReadOnly", "false");
                    body.Add("AccountInfo[CustFullName]", fullName);
                    body.Add("AccountInfo[CustMobilePhone]", phoneNumber);
                    body.Add("AccountInfo[Consultant]", consultant);
                    body.Add("AccountInfo[RefName]", refName);
                    var request = client.PostAsync($"{URI}/open-account/step/2", new FormUrlEncodedContent(body)).Result;
                    string response = request.Content.ReadAsStringAsync().Result.ToString();
                    var o = JObject.Parse(response);
                    bool isSuccess = o.SelectToken("success").Value<bool>();
                    if (isSuccess)
                    {
                        CustFullName = fullName;
                        CustMobilePhone = phoneNumber;
                        Consultant = consultant;
                        RefName = refName;
                        if (gender == 0)
                            Gender = "MALE";
                        else
                            Gender = "FEMALE";
                        return true;
                    }
                    else
                    {
                        Msg = o.SelectToken("message").Value<string>();
                        return false;
                    }
                }
            }
        }  
        public bool Step3()
        {
            Msg = string.Empty;
            using (HttpClientHandler handler = new HttpClientHandler())
            {

                handler.CookieContainer = new System.Net.CookieContainer();
                handler.UseCookies = false;
                handler.AutomaticDecompression = System.Net.DecompressionMethods.GZip | System.Net.DecompressionMethods.Deflate | System.Net.DecompressionMethods.None;

                if (string.IsNullOrEmpty(Proxy))
                {
                    var webProxy = new WebProxy(Proxy);
                    //{
                    //    username và password proxy nếu có
                    //    Credentials = new NetworkCredential("username", "password"),
                    //};
                    handler.UseProxy = true;
                    handler.Proxy = webProxy;
                }
                using (HttpClient client = new HttpClient(handler))
                {
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.TryAddWithoutValidation("accept-encoding", "gzip, deflate, br");
                    Dictionary<string, string> body = new Dictionary<string, string>();
                    body.Add("StepCount", "3");
                    body.Add("isNext", "true");
                    body.Add("RequestID", RequestID);
                    body.Add("IsReadOnly", "false");
                    body.Add("AccountInfo[CardFrontImage]", CardFrontImage);
                    body.Add("AccountInfo[CardBackImage]", CardBackImage);
                    body.Add("AccountInfo[CustMobilePhone]", CustMobilePhone);
                    var request = client.PostAsync($"{URI}/open-account/step/3", new FormUrlEncodedContent(body)).Result;
                    string response = request.Content.ReadAsStringAsync().Result.ToString();
                    var o = JObject.Parse(response);
                    bool isSuccess = o.SelectToken("success").Value<bool>();
                    if (isSuccess)
                    {
                        CardInfo(response);
                        return true;
                    }
                    else
                    {
                        Msg = o.SelectToken("message").Value<string>();
                        return false;
                    }
                }
            }
        }
        public bool Step4()
        {
            Msg = string.Empty;
            using (HttpClientHandler handler = new HttpClientHandler())
            {

                handler.CookieContainer = new System.Net.CookieContainer();
                handler.UseCookies = false;
                handler.AutomaticDecompression = System.Net.DecompressionMethods.GZip | System.Net.DecompressionMethods.Deflate | System.Net.DecompressionMethods.None;

                if (string.IsNullOrEmpty(Proxy))
                {
                    var webProxy = new WebProxy(Proxy);
                    //{
                    //    username và password proxy nếu có
                    //    Credentials = new NetworkCredential("username", "password"),
                    //};
                    handler.UseProxy = true;
                    handler.Proxy = webProxy;
                }
                using (HttpClient client = new HttpClient(handler))
                {
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.TryAddWithoutValidation("accept-encoding", "gzip, deflate, br");
                    Dictionary<string, string> body = new Dictionary<string, string>();
                    body.Add("StepCount", "4");
                    body.Add("isNext", "true");
                    body.Add("RequestID", RequestID);
                    body.Add("IsReadOnly", "false");
                    body.Add("AccountInfo[CustFullName]", CustFullName);
                    body.Add("AccountInfo[CustEmail]", CustEmail);
                    body.Add("AccountInfo[Consultant]", Consultant );
                    body.Add("AccountInfo[RefName]", RefName );
                    body.Add("AccountInfo[CustBornDate]", CustBornDate );
                    body.Add("AccountInfo[Gender]", Gender );
                    body.Add("AccountInfo[CardId]", CardId );
                    body.Add("AccountInfo[CardIssuedDate]", CardIssuedDate );
                    body.Add("AccountInfo[CardExpireDate]", CardExpireDate );
                    body.Add("AccountInfo[CardIssuedPlace]", CardIssuedPlace );
                    body.Add("AccountInfo[CustMobilePhone]", CustMobilePhone );
                    body.Add("AccountInfo[CustAddress]", CustAddress );
                    body.Add("AccountInfo[CustAddrProvince]", CustAddrProvince );
                    body.Add("AccountInfo[IpAccepted]", IpAccepted );
                    var request = client.PostAsync($"{URI}/open-account/step/4", new FormUrlEncodedContent(body)).Result;
                    string response = request.Content.ReadAsStringAsync().Result.ToString();
                    var o = JObject.Parse(response);
                    bool isSuccess = o.SelectToken("success").Value<bool>();
                    if (isSuccess)
                    {
                        
                        return true;
                    }
                    else
                    {
                        Msg = o.SelectToken("message").Value<string>();
                        return false;
                    }
                }
            }
        } 
        public bool UploadCMT(string path, bool isFrontImg = true)
        {
            string filename = Path.GetFileName(path);
            var client = new HttpClient();
            using (var multipartFormContent = new MultipartFormDataContent())
            {
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "multipart/form-data");
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "boundary=----WebKitFormBoundaryn15wrqsFvjFGLxDW");
                //client.DefaultRequestHeaders.TryAddWithoutValidation("accept-encoding", "gzip, deflate, br");
                //Add other fields
                //multipartFormContent.Add(new System.Net.Http.StringContent(message), name: "msg");
                //multipartFormContent.Add(new System.Net.Http.StringContent(sender), name: "sender");



                //Add the file
                if (!string.IsNullOrEmpty(path))
                {
                    var fileStreamContent = new System.Net.Http.StreamContent(File.OpenRead(path));
                    fileStreamContent.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
                    multipartFormContent.Add(fileStreamContent, name: filename, fileName: filename);

                }

                //Send it
                //httpclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("multipart/form-data"));
                var response = client.PostAsync($"https://openaccount.vps.com.vn/open-account/upload-multipart-image", multipartFormContent).Result;
                response.EnsureSuccessStatusCode();
                string result = response.Content.ReadAsStringAsync().Result.ToString();
                var o = JObject.Parse(result);
                bool isSuccess = o.SelectToken("success").Value<bool>();
                if (isSuccess)
                {
                    if (isFrontImg)
                        CardFrontImage = o.SelectToken("imgName").Value<string>();
                    else
                        CardBackImage = o.SelectToken("imgName").Value<string>();
                    return true;
                }
            }
            return false;
        }
    }
}
