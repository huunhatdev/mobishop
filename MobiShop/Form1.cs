﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using RegMobifone;
using static MobiShop.pareProfile;
using static MobiShop.Package;

namespace MobiShop
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void btn_Login_Click(object sender, EventArgs e)
        {
            bool hasToken = false;
            string token = string.Empty;
            exMobiShop.Login(tbUsername.Text, tbPassword.Text, out hasToken, out token);
            if (hasToken)
                tbToken.Text = token;
            else
                MessageBox.Show(token);
        }

        private void btnProfile_Click(object sender, EventArgs e)
        {
            string response = exMobiShop.getProfile(tbToken.Text);
            if (string.IsNullOrEmpty(response))
                MessageBox.Show("Lỗi xác thực thông tin");
            else
            {
                Profile profile = JsonConvert.DeserializeObject<Profile>(response);
                lbId.Text = profile.data.id.ToString();
                lbName.Text = profile.data.name;
                lbComission.Text = profile.data.commissionPrice;
            }
         
        }

        private void btnSendPakage_Click(object sender, EventArgs e)
        {
            string token = tbToken.Text ;
            string agent = (lbId.Text.ToString() == "000") ? null : lbId.Text;
            string phone = "0772000089";
            string package = "c90n";

            if (string.IsNullOrEmpty(token) || string.IsNullOrEmpty(agent) || string.IsNullOrEmpty(phone) || string.IsNullOrEmpty(package))
                MessageBox.Show("Điền đầy đủ thông tin");
            else
            {
                bool isSuccess = false;
                string msg = string.Empty;
               msg =  exMobiShop.sendPackage(phone, package.ToUpper(), agent, token, out isSuccess);
                if (isSuccess)
                {
                    string orderId = msg;
                    MessageBox.Show("Giới thiệu thành công");
                }else
                    MessageBox.Show(msg);
            }
                
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //VPS vps = new VPS();
            //vps.UploadCMT($"C:\\Users\\nhatl\\Downloads\\cmnd\\cmnd\\023992524_636_4.jpg");
            AnyCaptcha.SpeechToText(@"D:\tool\mymobi\Vasmobifone\RegMobi 2.0\bin\Debug\ghiam\2_e6c6305780b7.wav");
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
