﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace MobiShop
{
    class AnyCaptcha
    {

        public static void SpeechToText(string filePath)
        {
            try
            {
                var httpclient = new HttpClient();
                using (var multipartFormContent = new MultipartFormDataContent())
                {
                    //Add other fields
                    multipartFormContent.Add(new System.Net.Http.StringContent("vi-VN"), name: "LanguageCode");
                    //multipartFormContent.Add(new System.Net.Http.StringContent(sender), name: "sender");
                    //multipartFormContent.Add(new System.Net.Http.StringContent(chothuesim[phone]), name: "id");
                    //multipartFormContent.Add(new System.Net.Http.StringContent(otp), name: "code");
                    //multipartFormContent.Add(new System.Net.Http.StringContent(audio), name: "audio");
                    //multipartFormContent.Add(new System.Net.Http.StringContent(Form1.remote.tbApiKey.Text), name: "apikey");


                    //Add the file
                    if (!string.IsNullOrEmpty(filePath))
                    {
                        string fileName = Path.GetFileName(filePath);
                        var fileStreamContent = new System.Net.Http.StreamContent(File.OpenRead(filePath));
                        fileStreamContent.Headers.ContentType = new MediaTypeHeaderValue("audio/wav");
                        multipartFormContent.Add(fileStreamContent, name: fileName, fileName: fileName);

                    }

                    //Send it
                    //httpclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("multipart/form-data"));
                    var response = httpclient.PostAsync($"https://labs.anycaptcha.com/SpeechToTextFromBase64", multipartFormContent).Result;
                    response.EnsureSuccessStatusCode();
                    string result = response.Content.ReadAsStringAsync().Result.ToString();
                    string status = JObject.Parse(result).SelectToken("status").Value<string>();
                    string data = JObject.Parse(result).SelectToken("data").Value<string>();
                    if (status == "200")
                    {
                       
                    }
                    else
                    {
                      
                    }
                }
            }
            catch { }

        }

    }
}
